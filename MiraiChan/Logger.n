﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace MiraiChan {
    module Logger {
        private LOGFILE_PATH : string = "log.txt";
        
        public LogLine(message : string) : void {
            File.AppendAllText(LOGFILE_PATH, DateTime.Now.ToString() + ": ");
            File.AppendAllText(LOGFILE_PATH, message);
            File.AppendAllText(LOGFILE_PATH, "\n");
        }
        public LogException(ex : Exception) : void {
            LogLine(ex.Message + "\n" + ex.StackTrace);
        }
    }
}
