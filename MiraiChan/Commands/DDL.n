﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

using RestSharp;

namespace MiraiChan.Commands {
    class DDL {
        private class DDLItem {
            public filename : string { get; set; }
            public link : string { get; set; }
        }
        private client : RestClient;
        
        public this() {
            this.client = RestClient("http://mfs-ddl.de/ddl/");
            this.client.AddHandler("*", RestSharp.Deserializers.JsonDeserializer());
        }
        
        public GetDDLs(search : string) : string {
            def req = RestRequest("/content_json.txt", Method.GET);
            req.RequestFormat = DataFormat.Json;
            def resp = this.client.Execute.[List[DDLItem]](req);
            
            if (resp.Data == null ||resp.Data.Count == 0) {
                "Kyaa~! Unsere DDL-Seite scheint gerade nicht erreichbar zu sein. >_<"
            }
            else {
                def searchTerms = search.Split(array[" "], StringSplitOptions.RemoveEmptyEntries).Select(x => x.ToLower()).ToList();
                
                match (searchTerms.Count) {
                    | 0 => "Bitte gib einen Suchbegriff ein, Goshujin-sama! <3"
                    | _ => {
                        def ddls = resp.Data.Where(x => searchTerms.All(y => x.filename.ToLower().Contains(y))).ToList();
                
                        match (ddls.Count()) {
                            | 0 => "Kyaa, hazukashii! Mirai-chan konnte für den angegebenen Suchbegriff keinen Download finden! >_<"
                            | _ => {
                                mutable ret = "";
                    
                                foreach (ddl in ddls.OrderBy(x => x.filename)) {
                                    ret += $"$(ddl.filename): $(ddl.link)\n";
                                }
                    
                                ret
                            }
                        }
                    }
                }
            }
        }
    }
}
