﻿using System.Linq;

using MiraiChan.DataAccess;

namespace MiraiChan.Commands {
    class Opinion {
        private db : Database;
        
        public this(db : Database) {
            this.db = db;
        }
        
        public GetOpinion(topic : string) : string {
            def t = topic.ToLower();
            def predef = this.db.SelectOpinion(t);
            
            match (predef) {
                | null => {
                    def opinion = t.Select(x => x :> int).Sum() % 10;

                    match (opinion) {
                        | 0 => $"Mirai-chan findet $topic total behindert.";
                        | 1 => $"Mirai-chan hält $topic für voll scheiße."
                        | 2 => $"Mirai-chan ist $topic ziemlich abgeneigt."
                        | 3 => $"Mirai-chan findet $topic ganz okay."
                        | 4 => $"Mirai-chan hält nicht viel von $topic."
                        | 5 => $"Mirai-chan findet $topic voll doof! >_<"
                        | 6 => $"Mirai-chan hat keine richtige Meinung zu $topic."
                        | 7 => $"Mirai-chan ist total begeistert von $topic! <3"
                        | 8 => $"Mirai-chan mag $topic. ^_^"
                        | 9 => $"Mirai-chan steht voll auf $topic! <3" 
                        | _ => ""
                    }
                }
                | _ => predef.Opinion
            }
        }
    }
}
