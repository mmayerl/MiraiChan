﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

using MiraiChan.DataAccess;
using MiraiChan.DiscordAPI;

namespace MiraiChan.Commands {
    class Admin {
        private db : Database;
        private config : Config;
        private discord : DiscordInterface;
        
        public this(db : Database, config : Config, discord : DiscordInterface) {
            this.db = db;
            this.config = config;
            this.discord = discord;
        }
        
        internal Config(message : Message) : string {
            if (message.Content == "!config") {
                this.config.GetAsString()
            }
            else {
                def user = this.db.SelectSpecialUser(message.AuthorID);
                
                if (user != null && user.IsOwer) {
                    def p = message.Content.Split(array[" "], StringSplitOptions.RemoveEmptyEntries);
                        
                    match (p.Length) {
                        | 3 => {
                            mutable val;
                            if (int.TryParse(p[2], out val)) {
                                match (p[1].ToLower()) {
                                    | "interval" => { this.config.SetInterval(val); "Interval wurde aktualisiert." }
                                    | "requestinterval" => { this.config.SetRequestInterval(val); "RequestInterval wurde aktualisiert." }
                                    | "reminderinterval" => { this.config.SetReminderInterval(val); "ReminderInterval wurde aktualisiert." }
                                    | _ => "Dieser Wert kann nicht verändert werden. ~"
                                }
                            }
                            else {
                                $"Bist du zu blöd, eine Zahl einzugeben, $(message.AuthorName)?"
                            }
                        }
                        | _ => $"Hör auf, mich falsch zu bedienen, $(message.AuthorName)! =/"
                    }
                }
                else {
                    $"Du bist leider nicht berechtigt, an Mirai-chans Konfiguration rumzufummeln (kyaaaa~), $(message.AuthorName)!"
                }
            }
        }
        internal Query(message : Message) : string {
            def user = this.db.SelectSpecialUser(message.AuthorID);
            
            if (user != null && user.IsOwer) {
                def query = message.Content.Remove(0, 7);
                this.db.ExecuteQuery(query)
            }
            else {
                $"Dazu hast du keine Berechtigung, $(message.AuthorName)! >_<"
            }   
        }
        internal Join(message : Message) : string {
            def user = this.db.SelectSpecialUser(message.AuthorID);
            def ids = message.Content.Remove(0, 6).Trim();
            
            if (user != null && user.IsAdmin) {
                mutable id;
                
                if (long.TryParse(ids, out id)) {
                    def res = this.discord.TryAddChannel(id);
                    
                    match (res) {
                        | true => {
                            def n = ChannelRecord(id, false, false);
                            this.db.InsertChannel(n);
                            this.config.Channels.Add(n);
                            this.discord.Send(id, "Mirai-chan, toujou!");
                            $"Mirai-chan ist Channel $id beigetreten!"
                        }
                        | false => $"Channel $id konnte nicht hinzugefügt werden."
                    }
                }
                else {
                    $"Gib gefälligst die ID richtig ein, $(message.AuthorName)! >_<"
                }
            }
            else {
                $"Dazu hast du keine Berechtigung, $(message.AuthorName)! >_<"
            }   
        }
        internal Leave(message : Message) : string {
            def user = this.db.SelectSpecialUser(message.AuthorID);
            def ids = message.Content.Remove(0, 7).Trim();
            
            if (user != null && user.IsAdmin) {
                mutable id;
                
                if (long.TryParse(ids, out id)) {
                    def ch = this.config.Channels.Where(x => x.ID == id).FirstOrDefault();
                    
                    match (ch) {
                        | null => $"Channel $id konnte nicht entfernt werden, da Mirai-chan sich nicht in diesem Channel befindet."
                        | _ => {
                            this.discord.RemoveChannel(id);
                            _ = this.config.Channels.Remove(ch);
                            this.db.DeleteChannel(ch);
                            $"Channel $id wurde entfernt."
                        }
                    }
                }
                else {
                    $"Gib gefälligst die ID richtig ein, $(message.AuthorName)! >_<"
                }
            }
            else {
                $"Dazu hast du keine Berechtigung, $(message.AuthorName)! >_<"
            }
        }
        internal ChangeEro(message : Message) : string {
            def user = this.db.SelectSpecialUser(message.AuthorID);
            
            if (user != null && user.IsAdmin) {
                def ch = this.config.Channels.Where(x => x.ID == message.Channel).FirstOrDefault();
                
                ch.IsEro = !ch.IsEro;
                this.db.UpdateChannel(ch);
                
                "Der Ero-Status dieses Channels wurde geändert."
            }
            else {
                $"Dazu hast du keine Berechtigung, $(message.AuthorName)! >_<"
            }
        }
        internal BlockList(message : Message) : string {
            def user = this.db.SelectSpecialUser(message.AuthorID);
            def blocked = this.db.SelectSpecialUsers().Where(x => x.IsBlocked).ToList();
            
            if (user != null && user.IsAdmin) {
                mutable ret = "Blockierte Benutzer:\n";
                
                foreach (b in blocked) {
                    ret += $"    $(b.ID)\n";
                }
                otherwise {
                    ret += "    Keine blockierten Benutzer vorhanden.";
                }
                
                ret
            }
            else {
                $"Dazu hast du keine Berechtigung, $(message.AuthorName)! >_<"
            }
        }
        internal Block(message : Message) : string {
            def user = this.db.SelectSpecialUser(message.AuthorID);
            def ids = message.Content.Remove(0, 7).Trim();
            mutable id;
            
            if (user != null && user.IsAdmin) {
                if (long.TryParse(ids, out id)) {
                    def target = this.db.SelectSpecialUser(id);
                    
                    match (target) {
                        | null => {
                            this.db.InsertSpecialUser(SpecialUsersRecord(id, false, false, true));
                            $"User $id wurde blockiert."
                        }
                        | t when t.IsAdmin || t.IsOwer => $"Einen Admin kann man nicht blockieren, $(message.AuthorName)! >_<"
                        | t => {
                            target.IsBlocked = true;
                            this.db.UpdateSpecialUser(target);
                            $"User $id wurde blockiert."
                        }
                    }
                }
                else {
                    $"Gib gefälligst die ID richtig ein, $(message.AuthorName)! >_<"
                }
            }
            else {
                $"Dazu hast du keine Berechtigung, $(message.AuthorName)! >_<"
            }
        }
        internal UnBlock(message : Message) : string {
            def user = this.db.SelectSpecialUser(message.AuthorID);
            def ids = message.Content.Remove(0, 9).Trim();
            mutable id;
            
            if (user != null && user.IsAdmin) {
                if (long.TryParse(ids, out id)) {
                    def target = this.db.SelectSpecialUser(id);
                    
                    match (target) {
                        | null => $"Der User $id kann nicht freigegeben werden, da er nicht blockiert ist."
                        | t when !t.IsBlocked => $"Der User $id kann nicht freigegeben werden, da er nicht blockiert ist."
                        | t => {
                            target.IsBlocked = false;
                            this.db.UpdateSpecialUser(target);
                            $"User $id wurde freigegeben."
                        }
                    }
                }
                else {
                    $"Gib gefälligst die ID richtig ein, $(message.AuthorName)! >_<"
                }
            }
            else {
                $"Dazu hast du keine Berechtigung, $(message.AuthorName)! >_<"
            }
        }
    }
}
