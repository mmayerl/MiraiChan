﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

using MiraiChan.DataAccess;

namespace MiraiChan.Commands {
    class PraiseAndInsult {
        private db : Database;
        
        public this(db : Database) {
            this.db = db;
        }
        
        public Praise(person : string) : string {
            def cur = this.db.SelectPraisesAndInsults(person);
                
            match (cur) {
                | null => this.db.UpdatePraisesAndInsults(PraisesAndInsultsRecord(person, 0, 1))
                | _  =>   this.db.UpdatePraisesAndInsults(PraisesAndInsultsRecord(person, cur.Insults, cur.Praises + 1))
            }
                
            $"Mirai-chan findet das total super von dir, $person!"
        }
        public Insult(person : string) : string {
            def cur = this.db.SelectPraisesAndInsults(person);
                
            match (cur) {
                | null => this.db.UpdatePraisesAndInsults(PraisesAndInsultsRecord(person, 1, 0))
                | _  =>   this.db.UpdatePraisesAndInsults(PraisesAndInsultsRecord(person, cur.Insults + 1, cur.Praises))
            }
                
            $"Mirai-chan verachtet dich dafür, $person!"
        }
        public GetPraisesAndInsults(person : string) : string {
            def cur = this.db.SelectPraisesAndInsults(person);
                
            match (cur) {
                | null => $"$person wurde von Mirai-chan noch nie gelobt oder verachtet."
                | _ =>    $"Mirai-chan hat $person bisher $(cur.Praises) Mal gelobt und $(cur.Insults) Mal verachtet!"
            }
        }
    }
}
