﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using RestSharp;
using RestSharp.Authenticators;

namespace MiraiChan.Commands {
    class AnimeInfo {
        //Private type for communicating with the API
        private class Entry {
            public title : string { get; set; }
            public episodes : int { get; set; }
            public synopsis : string { get; set; }
        }
        //Custom deserializer for preprocessing of the returned XML
        private class MALDeserializer : RestSharp.Deserializers.XmlDeserializer {
            public override Deserialize[T](response : RestSharp.IRestResponse) : T {
                response.Content = HttpUtility.HtmlDecode(response.Content);
                response.Content = HttpUtility.HtmlDecode(response.Content);
                response.Content = response.Content.Replace("&", "");
                base.Deserialize(response)
            }
        }
        
        private MAL_API : string = "api/anime/search.xml";
        private config : Config;
        private client : RestClient;
        
        public this(config : Config, password : string) {
            this.config = config;
            this.client = RestClient(config.MALServer);
            this.client.Authenticator = HttpBasicAuthenticator(config.MALUser, password);
            this.client.AddHandler("application/xml", MALDeserializer());
            this.client.AddHandler("text/xml", MALDeserializer());
            this.client.AddHandler("*", MALDeserializer());
        }
        
        public GetAnimeInfo(title : string) : string {
            try {
                def req = RestRequest(this.MAL_API, Method.GET);
                _ = req.AddParameter("q", title);
                def resp = this.client.Execute.[List[Entry]](req);            
            
                if (resp.Data == null || resp.Data.Count == 0 ) {
                    "Über diesen Anime kann ich keine Informationen finden. =("
                }
                else {
                    def entry = resp.Data[0];
                    $"**Titel**: $(entry.title)\n**Episoden**: $(entry.episodes)\n**Kurzinfo**:\n```$(entry.synopsis)```"
                }
            }
            catch {
                | ex => {
                    Logger.LogException(ex);
                    "Huch?! Da ist Mirai-chan irgendein Missgeschick passiert! Hier stimmt etwas nicht! Erzählt ihrem Schöpfer-sama davon!"
                }
            }
        }
    }
}