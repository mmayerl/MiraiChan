﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using RestSharp;

namespace MiraiChan.Commands {
    class Topic {
        private class Item {
            public title : string { get; set; }
            public link : string { get; set; }
        }
        
        private URL : string = "http://animenewsnetwork.com";
        private API : string = "/all/rss.xml";
        private client : RestClient;
        private rnd : Random;
        
        public this() {
            this.client = RestClient(URL);
            this.rnd = Random();
        }
        
        public GetRandomNews() : string {
            try {
                def req = RestRequest(API, Method.GET);
                def resp = this.client.Execute.[List[Item]](req);
            
                match (resp) {
                    | null | x when x.Data.Count == 0  => "FEHLER: Mirai-chan konnte keine Daten abrufen. Möglicherweise ist die Datenquelle nicht erreichbar. =/"
                    | x => {
                        x.Data[this.rnd.Next(0, x.Data.Count)].link
                    }
                }
            }
            catch {
                | ex => {
                    Logger.LogException(ex);
                    "FEHLER: Huch?! Da ist Mirai-chan irgendein Missgeschick passiert! Hier stimmt etwas nicht! Erzählt ihrem Schöpfer-sama davon!"
                }
            }
        }
    }
}