﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

using MiraiChan.DataAccess;

namespace MiraiChan.Commands {
    class Remind {
        private db : Database;
        private reminders : List[ReminderRecord];
        
        public this(db : Database) {
            this.db = db;
            this.reminders = List();
            
            //Get reminders from the database
            this.reminders.AddRange(this.db.SelectReminders());
        }
        
        public SetReminder(channel : long, at : DateTime, text : string, from : string) : void {
            //Add new reminder record to the database
            def r = ReminderRecord(0, channel, at, text, from);
            this.db.InsertReminder(r);
            
            lock (this) {
                //Reload in-memory list from database to update reminder IDs correctly
                this.reminders.Clear();
                this.reminders.AddRange(this.db.SelectReminders());
            }
        }
        public GetCurrentReminders() : IEnumerable[ReminderRecord] {
            lock (this) {
                //Get reminders from the in-memory list
                def curReminders = this.reminders.Where(x => x.At < DateTime.Now).ToList();
            
                //Delete all current reminders from the database and from the in-memory list
                foreach (reminder in curReminders) {
                    _ = this.reminders.Remove(reminder);
                    this.db.DeleteReminder(reminder.ID);
                }
            
                //Return reminders
                curReminders
            }
        }
    }
}
