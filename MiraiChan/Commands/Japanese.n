﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Nemerle.Extensions;
using MiraiChan.DataAccess;

namespace MiraiChan.Commands {
    class Japanese {
        private db : Database;
        
        public this(db : Database) {
            this.db = db;
        }
        
        public LookupWord(searchString : string) : string {
            def entries = this.db.SelectDictionaryEntries(searchString);
            
            if (entries.Count() > 0) {
                string.Join(Environment.NewLine, entries.Distinct().OrderBy(x => x.Surface).Take(10).Select(x => $"$(x.Surface)          $(x.Reading)          $(x.Meaning)"))
            }
            else {
                "Gomennasai, goshujin-sama! Mirai-chan kennt solche komplizierten Wörter nicht. >_<"
            }
        }
    }
}
