﻿using System;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;

namespace MiraiChan.Commands {
    class Image {
        private POST_URL : string = "http://yande.re/post?tags={0}+order:random";
        private IMAGE_URL : string = "https://files.yande.re/image/";
        private LOLI_TAG : string = "loli";
            
        private client : WebClient;
        private regPost : Regex;
        private regImg : Regex;
        private regSample : Regex;
        private rnd : Random;

        public this() {
            this.client = WebClient();
            this.regPost = Regex("<span class=\"plid\">#pl https://yande.re/post/show/[^<]*");
            this.regImg = Regex("\"https://files.yande.re/image/[^\"]*");
            this.regSample = Regex("\"https://files.yande.re/sample/[^\"]*");
            this.rnd = Random();
        }

        private GetPost(tag : string) : string {
            def data = client.DownloadData(string.Format(POST_URL, tag));
            def source = Encoding.UTF8.GetString(data);
            def mat = regPost.Matches(source);
            
            match (mat.Count) {
                | 0 => ""
                | _ => mat[rnd.Next(0, mat.Count)].Value.Remove(0, 23)
            }
        }
        public GetRandomImage(tag : string) : string {
            try {
                def post = this.GetPost(tag);
                
                match (post) {
                    | "" => "Zu dem angegebenen Tag scheint es kein Bild zu geben. =("
                    | _ => {
                        def data = this.client.DownloadData(post);
                        def source = Encoding.UTF8.GetString(data);
                        def mat = this.regImg.Matches(source);
            
                        match (mat.Count) {
                            //There was no full-size image on page - use the sample image (this is done
                            //if there is no higher resolution than the one shown on the /post page available).
                            | 0 => {
                                def sample = this.regSample.Matches(source)[0]; //Just assume there is one. If not, we will get an exception, which is what we want at this point.
                                sample.Value.Remove(0, 1)
                            }
                            //There was a full-size image - use it.
                            | _ => {
                                def img = mat[0];
                                img.Value.Remove(0, 1)
                            }
                        } 
                    }
                }
            }
            catch {
                | ex => {
                    Logger.LogException(ex);
                    "Huch?! Da ist Mirai-chan irgendein Missgeschick passiert! Hier stimmt etwas nicht! Erzählt ihrem Schöpfer-sama davon!"
                }
            }
        }
        public GetRandomLoli() : string {
            GetRandomImage(LOLI_TAG)
        }
    }
}
