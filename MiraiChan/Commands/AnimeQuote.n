﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.IO;
using System.Net;
using MiraiChan.DataAccess;
using SubtitleLib.ASS;

namespace MiraiChan.Commands {
    class AnimeQuote {
        private db : Database;

        public this(db : Database) {
            this.db = db;
        }

        public GetRandomQuote() : string {
            this.GetRandomQuoteForSeries("%")
        }
        public GetRandomQuoteForSeries(series : string) : string {
            def quote = this.db.SelectRandomAnimeQuote(series);
            
            match (quote) {
                | null => "Gomennasai, goshujin-sama! Für diese Serie hat Mirai-chan kein Zitat gespeichert. >_<"
                | _ => $"```$(quote.Text)``` - $(quote.Series), Episode $(quote.Episode)"
            }
        }
        
        public AddLocalFile(file : string, series : string, episode : int) : bool {
            try {
                def ass = ASSFile(file);
                foreach (e in ass.EventLines.Where(x => x.TextWithoutTags.Length > 5)) {
                    db.InsertAnimeQuote(AnimeQuoteRecord(0, series, episode, e.TextWithoutTags));
                }
                
                true
            }
            catch {
                | ex => {
                    Logger.LogException(ex);
                    false
                }
            }
        }
    }
}
