﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Nemerle.Extensions;
using MiraiChan.DataAccess;

namespace MiraiChan.Commands {
    class Quote {
        private db : Database;

        public this(db : Database) {
            this.db = db;
        }

        public GetRandomQuote() : string {
            def quote = this.db.SelectRandomQuote("%");
            $"```$(quote.Text)``` - $(quote.Author), hinzugefügt von $(quote.AddedBy)\n"
        }
        public GetRandomQuoteForPerson(person : string) : string {
            def quote = this.db.SelectRandomQuote(person);
            
            match (quote) {
                | null => "Gomennasai, goshujin-sama! Für diese Person hat Mirai-chan kein Zitat gespeichert. >_<"
                | _ => $"```$(quote.Text)``` - $(quote.Author), hinzugefügt von $(quote.AddedBy)\n"
            }
        }
        public AddQuote(author : string, text : string, user : string) : void {
            this.db.InsertQuote(QuoteRecord(0, author, text, user));
        }
    }
}
