﻿using Nemerle.Extensions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Threading;
using System.IO;
using System.Console;

using MiraiChan.Commands;
using MiraiChan.DataAccess;
using MiraiChan.DiscordAPI;

namespace MiraiChan {
    class Program {
        //Commands
        private db : Database;
        private config : Config;
        private admin : Admin;
        private animeQuotes : AnimeQuote;
        private news : Topic;
        private image : Image;
        private opinion : Opinion;
        private quotes : Quote;
        private wadoku : Japanese;
        private praiseAndInsult : PraiseAndInsult;
        private remind : Remind;
        private anime : AnimeInfo;
        private ddl : DDL;
        
        //Interface to the Discord API
        private discord : DiscordInterface;
        
        //State
        private stats : Dictionary[string, MessageStatEntry];
        private mutable messageCount : int;
        private rnd : Random;

        static Main(args : array[string]) : void {
            match (args.Length) {
                | 0 => WriteLine("You have to specify what Mirai-chan should do.")
                | 2 => match(args[0]) {
                    | "run" => (Program(args[1])).Run()
                }
                //The password is more or less redundant for these commands, but who cares ...
                | 3 => match(args[0]) {
                    | "updatedb" => (Program(args[1])).UpdateDB(args[2])
                }
                | 5 => match(args[0]) {
                    | "addass" => (Program(args[1])).AddASS(args[2], args[3], int.Parse(args[4]))
                }
                | _ => WriteLine("You have specified too many parameters.")
            }
        }

        public this(password : string) {
            this.db = Database();
            this.config = Config(this.db);
            this.discord = DiscordInterface(this.config, password);
            
            this.admin = Admin(db, config, discord);
            this.animeQuotes = AnimeQuote(this.db);
            this.news = Topic();
            this.image = Image();
            this.opinion = Opinion(this.db);
            this.quotes = Quote(this.db);
            this.wadoku = Japanese(this.db);
            this.praiseAndInsult = PraiseAndInsult(this.db);
            this.remind = Remind(this.db);
            this.anime = AnimeInfo(this.config, password);
            this.ddl = DDL();
            
            this.stats = Dictionary();
            this.rnd = Random();
        }
        private Run() : void {
            AppDomain.CurrentDomain.UnhandledException += (_, e) => {
                match (e.ExceptionObject) {
                    | Exception as ex => Logger.LogException(ex)
                    | _ => Logger.LogLine("An undefinied error occured: " + e.ExceptionObject.ToString());
                }
            };
            
            //Start background threads
            StartAutoQuoteThread();
            StartReminderThread();
            StartRequestThread();
            
            //Wait for exit
            _ = ReadLine();
            Environment.Exit(0);
        }
        private UpdateDB(file : string) : void {
            try {
                def text = File.ReadAllText(file);
                _ = this.db.ExecuteQuery(text);
            }
            catch {
                | _ is IOException => WriteLine("The specified file could not be read.");
                | ex               => WriteLine("An error occured.\n" + ex.Message);
            }
        }
        private AddASS(file : string, series : string, episode : int) : void {
            try {
                _ = this.animeQuotes.AddLocalFile(file, series, episode)
            }
            catch {
                | ex => WriteLine("An error occured.\n" + ex.Message);
            }
        }

        private StartAutoQuoteThread() : void {
            def thread = Thread(() => {
                while (true) {                 
                    Thread.Sleep(this.config.Interval);
                    
                    unless (DateTime.Now.Hour < 10) {
                        discord.Send(this.config.AutoQuoteChannel, this.quotes.GetRandomQuote());
                    }
                }
            });
            thread.Start();
        }
        private StartReminderThread() : void {
            def thread = Thread(() => {
                while (true) {
                    Thread.Sleep(this.config.ReminderInterval);
                    this.SendReminders();
                }
            });
            thread.Start();
        }
        private StartRequestThread() : void {
            def thread = Thread(() => {
                while (true) {
                    def messages = discord.GetNewMessages();

                    foreach (message in messages) {
                        WriteLine($"Received message on channel $(message.Channel) by $(message.AuthorName) ($(message.AuthorID)): $(message.Content)");
                        UpdateStats(message);
                        HandleCommand(message);
                    }
                
                    Thread.Sleep(this.config.RequestInterval);
                }
            });
            thread.Start();
        }
        
        private UpdateStats(message : Message) : void {
            unless (this.stats.ContainsKey(message.AuthorName)) {
                this.stats.Add(message.AuthorName, MessageStatEntry(message.AuthorName, DateTime.Now, 0, 0));
            }
            
            this.stats[message.AuthorName].MessageCount++;
            this.stats[message.AuthorName].XDCount += Regex.Matches(message.Content, "[xX]+[dD]+").Count;
            this.messageCount++;
        }
        
        private HandleCommand(message : Message) : void {
            when (message.Content.StartsWith("!")) {
                def user = this.db.SelectSpecialUser(message.AuthorID);
                
                when (user == null || !user.IsBlocked) {
                    def first = message.Content.Split(array[" "], StringSplitOptions.RemoveEmptyEntries)[0];
            
                    match (first) {
                        | "!help" => HandleHelp(message)
                        | "!quote" => HandleQuote(message)
                        | "!animequote" => HandleAnimeQuote(message)
                        | "!topic" => HandleTopic(message)
                        | "!image" => HandleImage(message)
                        | "!loli" => HandleLoli(message)
                        | "!coin" => HandleCoin(message)
                        | "!die" => HandleDie(message)
                        | "!japanese" => HandleJapanese(message)
                        | "!opinion" => HandleOpinion(message)
                        | "!insult" => HandleInsult(message)
                        | "!praise" => HandlePraise(message)
                        | "!like" => HandleLike(message)
                        | "!config" => HandleConfig(message)
                        | "!stats" => HandleStats(message)
                        | "!addquote" => HandleAddQuote(message)
                        | "!reminder" => HandleReminder(message)
                        | "!query" => HandleQuery(message)
                        | "!animeinfo" => HandleAnimeInfo(message)
                        | "!ddl" => HandleDDL(message)
                        | "!join" => HandleJoin(message)
                        | "!leave" => HandleLeave(message)
                        | "!changeero" => HandleChangeEro(message)
                        | "!blocklist" => HandleBlockList(message)
                        | "!block" => HandleBlock(message)
                        | "!unblock" => HandleUnBlock(message)
                        | _ => discord.Send(message.Channel, $"Diesen Befehl kenne ich leider nicht, $(message.AuthorName).");
                    }
                }
            }
        }
        private HandleHelp(message : Message) : void {
            def msg = string.Format("{0}\n\n{1}\n{2}\n{3}\n{4}\n{5}\n{6}\n{7}\n{8}\n{9}\n{10}\n{11}\n{12}\n{13}\n{14}\n{15}\n{16}\n{17}\n{18}",
                                        "Hier sind Mirai-chans aktuelle Befehle:",
                                        "    !help: Hilfe anzeigen.",
                                        "    !insult name: Bring Mirai-chan dazu, ein ganz unartiges Shoujo zu sein und die angegebene Person zu beleidigen.",
                                        "    !praise name: Bring Mirai-chan dazu, total yasashii zu sein und die angegebene Person zu homerun. <3",
                                        "    !like name: Lass Mirai-chan anzeigen, wie oft die angegebene Person gelobt/verachtet wurde.",
                                        "    !coin: Lass Mirai-chan eine Münze werfen.",
                                        "    !die: Lass Mirai-chan einen Würfel würfeln.",
                                        "    !quote [username]: Ein zufälliges Zitat von einem der Wahnsinnigen hier anzeigen.",
                                        "    !addquote person text: Füge ein Zitat zu Mirai-chans Zitatliste hinzu.",
                                        "    !animequote [serie]: Ein zufälliges Zitat aus einem Anime ausgeben.",
                                        "    !opinion topic: Frag Mirai-chan nach ihrer Meinung zum angegebenen Thema.",
                                        "    !topic: Bitte Mirai-chan um ein Diskussionsthema.",
                                        "    !japanese word: Lass Mirai-chan für dich das angegebene Wort nachschlagen (Präfixsuche). Maximal 10 Ergebnisse! <(^.^<)",
                                        "    !stats: Lass Mirai-chan ein paar Statistiken zum aktuellen Chat anzeigen.",
                                        "    (ero) !loli: Bring Mirai-chan dazu, ein zufälliges Bild einer Loli zu posten.",
                                        "    (ero) !image tag: Bring Mirai-chan dazu, ein zufälliges Bild zu dem angegebenen Tag zu posten.",
                                        "    !reminder time text: Bring Mirai-chan dazu, zur angegebenen Zeit (ISO 8601!) eine Erinnerung zu schicken.",
                                        "    !animeinfo titel: Bitte Mirai-chan um Informationen zum angegebenen Anime. (Quelle: MAL)",
                                        "    !ddl suchbegriffe: Durchsuche die DDL-Datenbank von MFS.");
            discord.Send(message.Channel, msg);
        }
        private HandleQuote(message : Message) : void {
            if (message.Content == "!quote") {
                discord.Send(message.Channel, this.quotes.GetRandomQuote());
            }
            else {
                def person = message.Content.Remove(0, 6).Trim();
                discord.Send(message.Channel, this.quotes.GetRandomQuoteForPerson(person));
            }
        }
        private HandleAnimeQuote(message : Message) : void {
            if (message.Content == "!animequote") {
                discord.Send(message.Channel, this.animeQuotes.GetRandomQuote());
            }
            else {
                def series = message.Content.Remove(0, 11).Trim();
                discord.Send(message.Channel, this.animeQuotes.GetRandomQuoteForSeries(series));
            }
        }
        private HandleTopic(message : Message) : void {
            discord.Send(message.Channel, string.Format("Euch ist langweilig? Mirai-chan hat ein Thema für euch: {0} ~", this.news.GetRandomNews()));
        }
        private HandleImage(message : Message) : void {
            if (this.config.Channels.Exists(x => x.ID == message.Channel && x.IsEro)) {
                def tag = message.Content.Remove(0, 6).Trim();
                this.discord.Send(message.Channel, this.image.GetRandomImage(tag));
            }
            else {
                this.discord.Send(message.Channel, $"Dieser Befehl steht nur in Ero-Channels zur Verfügung, $(message.AuthorName)!");
            }
        }
        private HandleLoli(message : Message) : void {
            if (this.config.Channels.Exists(x => x.ID == message.Channel && x.IsEro)) {
                this.discord.Send(message.Channel, this.image.GetRandomLoli());
            }
            else {
                this.discord.Send(message.Channel, $"Dieser Befehl steht nur in Ero-Channels zur Verfügung, $(message.AuthorName)!");
            }
        }
        private HandleCoin(message : Message) : void {
            def side = this.rnd.Next() % 2;
            discord.Send(message.Channel, string.Format("Mirai-chan wirft eine Münze für dich ~\nErgebnis: {0}.", if (side == 1)  "Kopf" else "Zahl"));
        }
        private HandleDie(message : Message) : void {
            def res = this.rnd.Next(1, 7);
            discord.Send(message.Channel, string.Format("Mirai-chan würfelt für dich ~\nErgebnis: {0}.", res));
        }
        private HandleJapanese(message : Message) : void {
            def word = message.Content.Remove(0, 9).Trim();
            
            if (!string.IsNullOrWhiteSpace(word)) {
                discord.Send(message.Channel, this.wadoku.LookupWord(word));
            }
            else {
                discord.Send(message.Channel, string.Format("Hör auf, mich falsch zu bedienen, {0}! =/", message.AuthorName));
            }
        }
        private HandleOpinion(message : Message) : void {
            def topic = message.Content.Remove(0, 8).Trim();
            
            if (!string.IsNullOrWhiteSpace(topic)) {
                discord.Send(message.Channel, this.opinion.GetOpinion(topic));
            }
            else {
                discord.Send(message.Channel, string.Format("Hör auf, mich falsch zu bedienen, {0}! =/", message.AuthorName));
            }
        }
        private HandleInsult(message : Message) : void {
            def person = message.Content.Remove(0, 7).Trim();
            
            if (!string.IsNullOrWhiteSpace(person)) {
                this.discord.Send(message.Channel, this.praiseAndInsult.Insult(person));
            }
            else {
                discord.Send(message.Channel, $"Hör auf, mich falsch zu bedienen, $(message.AuthorName)! =/");
            }
        }
        private HandlePraise(message : Message) : void {
            def person = message.Content.Remove(0, 7).Trim();
            
            if (!string.IsNullOrWhiteSpace(person)) {
                this.discord.Send(message.Channel, this.praiseAndInsult.Praise(person));
            }
            else {
                discord.Send(message.Channel, $"Hör auf, mich falsch zu bedienen, $(message.AuthorName)! =/");
            }
        }
        private HandleLike(message : Message) : void {
            def person = message.Content.Remove(0, 5).Trim();
            
            if (!string.IsNullOrWhiteSpace(person)) {
                this.discord.Send(message.Channel, this.praiseAndInsult.GetPraisesAndInsults(person));
            }
            else {
                this.discord.Send(message.Channel, $"Hör auf, mich falsch zu bedienen, $(message.AuthorName)! =/");
            }
        }
        private HandleStats(message : Message) : void {
            mutable msg = "";
            
            foreach (entry in this.stats) {
                msg += string.Format("{0}:\n    Anzahl XDs: {1}\n    Anzahl Nachrichten: {2}\n    Zuerst gesehen: {3:dd.MM.yyyy HH:mm:ss}\n    Anzahl XDs/h: {4:N2}\n    Anzahl Nachrichten/h: {5:N2}\n",
                                        entry.Value.Author,
                                        entry.Value.XDCount,
                                        entry.Value.MessageCount,
                                        entry.Value.FirstSeen,
                                        entry.Value.XDPerHour,
                                        entry.Value.MessagesPerHour); 
            }
            
            msg += $"\nGesamt Nachrichten: $(this.messageCount)";
            
            discord.Send(message.Channel, msg);
        }
        private HandleAddQuote(message : Message) : void {
            def content = message.Content.Remove(0, 10);
            def reg = Regex(" ");
            def parts = reg.Split(content, 2);
            
            match (parts.Length) {
                | 2 => {
                    this.quotes.AddQuote(parts[0], parts[1], message.AuthorName);
                    this.discord.Send(message.Channel, $"Das Zitat \"$(parts[0]): $(parts[1])\" wurde von $(message.AuthorName) hinzugefügt.");
                }
                | _ => this.discord.Send(message.Channel, "Gib den Befehl gefälligst richtig ein, Baka! >_<");
            }
        }
        private HandleReminder(message : Message) : void {
            def content = message.Content.Remove(0, 10);
            def reg = Regex(" ");
            def parts = reg.Split(content, 2);
            
            match (parts.Length) {
                | 2 => {
                    mutable at;
                    match (DateTime.TryParseExact(parts[0], "s", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out at)) {
                        | true => {
                            this.remind.SetReminder(message.Channel, at, parts[1], message.AuthorName);
                            this.discord.Send(message.Channel, $"Reminder \"$(parts[1])\" von $(message.AuthorName) wurde hinzugefügt.");
                        }
                        | false => this.discord.Send(message.Channel, "Kyaaaaa~! Ein falsches Datum! Kreeeeiiiiisch! >_< Du musst ISO 8601 verwenden: YYYY-MM-DDThh:mm:ss!")
                    }
                }
                | _ => this.discord.Send(message.Channel, "Gib den Befehl gefälligst richtig ein, Baka! >_<")
            }
        }
        private HandleAnimeInfo(message : Message) : void {
            def title = message.Content.Remove(0, 11).Trim();
            this.discord.Send(message.Channel, this.anime.GetAnimeInfo(title));
        }
        private HandleDDL(message : Message) : void {
            def search = message.Content.Remove(0, 4).Trim();
            this.discord.Send(message.Channel, this.ddl.GetDDLs(search));
        }
        
        private HandleJoin(message : Message) : void {
            this.discord.Send(message.Channel, this.admin.Join(message));
        }
        private HandleLeave(message : Message) : void {
            this.discord.Send(message.Channel, this.admin.Leave(message));
        }
        private HandleChangeEro(message : Message) : void {
            this.discord.Send(message.Channel, this.admin.ChangeEro(message));
        }
        private HandleBlockList(message : Message) : void {
            this.discord.Send(message.Channel, this.admin.BlockList(message));
        }
        private HandleBlock(message : Message) : void {
            this.discord.Send(message.Channel, this.admin.Block(message));
        }
        private HandleUnBlock(message : Message) : void {
            this.discord.Send(message.Channel, this.admin.UnBlock(message));
        }
        private HandleConfig(message : Message) : void {
            this.discord.Send(message.Channel, this.admin.Config(message));
        }
        private HandleQuery(message : Message) : void {
            this.discord.Send(message.Channel, this.admin.Query(message));
        }
        
        private SendReminders() : void {
            def reminders = this.remind.GetCurrentReminders();
             
            foreach (reminder in reminders) {
                this.discord.Send(reminder.Channel, $"**Erinnerung von $(reminder.From): $(reminder.Text)**");
            }
        }
    }
}
