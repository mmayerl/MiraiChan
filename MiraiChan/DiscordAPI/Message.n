﻿namespace MiraiChan.DiscordAPI {
    class Message {
        public Channel : long { get; set; }
        public AuthorName : string { get; set; }
        public AuthorID : long { get; set; }
        public Content : string { get; set; }
    }
}
