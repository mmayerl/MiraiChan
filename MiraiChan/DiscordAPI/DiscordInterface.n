﻿using Nemerle.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Net;
using RestSharp;
using WebSocketSharp;
using MiraiChan.DiscordAPI.Messages;

namespace MiraiChan.DiscordAPI {
    class DiscordInterface {
        private config : Config;
        
        private MessageAPI : string = "api/channels/{0}/messages";
        private LoginAPI : string  ="api/auth/login";
        private GatewayAPI : string = "api/gateway";
        private Epoch : DateTime = DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        
        private client : RestClient;
        private mutable ws : WebSocket;
        private lastIDs : Dictionary[long, long];
        private mutable token : string;
        
        public this(config : Config, password : string) {
            this.lastIDs = Dictionary();
            this.config = config;
            
            this.client = RestClient(config.Server);
            this.
                
            Login(password);
            CreateSocket();
            InitMessageID();
        }
        
        private Login(password : string) : void {
            //Login
            def loginReq = RestRequest(this.LoginAPI, Method.POST);
            loginReq.RequestFormat = DataFormat.Json;
            _ = loginReq.AddBody(LoginRequest() <- { 
                email = config.User; 
                password = password
            });

            def loginResp = this.client.Execute.[LoginResponse](loginReq);

            when (loginResp.Data == null || loginResp.Data.token == null) {
                Console.WriteLine("Error: Login failed.");
                Environment.Exit(-1);
            }
            
            //Set authentication token
            this.token = loginResp.Data.token;
            this.client.AddDefaultHeader("authorization", loginResp.Data.token);
        }
        
        private CreateSocket() : void {
           //Get gateway URL
            def gatewayReq = RestRequest(this.GatewayAPI, Method.GET);
            def gatewayResp  =this.client.Execute.[GatewayResponse](gatewayReq);
            
            when (gatewayResp.Data == null) {
                Console.WriteLine("Error: Could not connect to gateway.");
                Environment.Exit(-1);
            }
            
            //Open websocket to gateway
            this.ws = WebSocket(gatewayResp.Data.url);
            ws.EnableRedirection = true;
            ws.Log.File = "ws.txt";
            ws.OnOpen += WebSocket_Open;
            
            ws.Connect();
        }
        private WebSocket_Open(_ : object, _ : EventArgs) : void {
            //Once the websocket is open, we send an init message
            def msg = <#{ 
                            "op": 2, 
                            "d": { 
                            "token": "&tok", 
                                "v": 3, 
                                "properties": {
                                    "$os": "Windows",
                                    "$browser": "Chrome",
                                    "$device": "",
                                    "$referrer": "https://discordapp.com/@me",
                                    "$referring_domain": "discordapp.com"
                                } 
                            } 
                        }#>.Replace("&tok", this.token);
            this.ws.Send(msg);
            
            //Start timer for sending the mendatory keep-alive message
            def timer = Timer();
            timer.Elapsed += WebSocketKeepAlive;
            timer.Interval = 41250; //Default heartbeat interval
            timer.Enabled = true;
        }
        private WebSocketKeepAlive(_ : object, _ : ElapsedEventArgs) : void {
            def timestamp = (DateTime.UtcNow - this.Epoch).TotalMilliseconds :> long;
            def msg = <# {
                            "op": 1,
                            "d": "&timestamp"
                      }#>.Replace("&timestamp", timestamp.ToString());
                      
            this.ws.Send(msg);
        }
        
        public Send(channel : long, message : string) : void {
            def msg = if (message.Length > 2000) message.Substring(0, 1980) + "\n... (abgeschnitten)" else message;
            
            def sendReq = RestRequest(string.Format(this.MessageAPI, channel), Method.POST);
            sendReq.RequestFormat = DataFormat.Json;
            _ = sendReq.AddBody(SendMessageRequest() <- { content = msg });
            _ = client.Execute(sendReq);
        }
        
        private SetMessageID(channel : long) : bool {
            def req = RestRequest(string.Format(this.MessageAPI, channel), Method.GET);
            _ = req.AddParameter("limit", 1);
            def resp = client.Execute.[List[GetMessageResponse]](req);
                
            //Only add if channel was reachable and there were messages already there
            if (resp.StatusCode == HttpStatusCode.OK && resp.Data.Count > 0 && resp.Data[0].id != 0) {
                this.lastIDs[channel] = resp.Data[0].id;
                true
            }
            else {
                false
            }
        }
        private InitMessageID() : void {
            //Get the IDs of the last message for every channel
            foreach (channel in this.config.Channels) {
                _ = SetMessageID(channel.ID);
            }
        }
        internal TryAddChannel(channel : long) : bool {
            SetMessageID(channel)
        }
        internal RemoveChannel(channel : long) : void {
            _ = this.lastIDs.Remove(channel);
        }
        
        public GetNewMessages() : IEnumerable[Message] {
            def ret = List();

            foreach (channel in this.lastIDs.Select(x => x.Key).ToArray()) { //We make a copy here to avoid invalid operation
                def getReq = RestRequest(string.Format(this.MessageAPI, channel), Method.GET);
                _ = getReq.AddParameter("after", this.lastIDs[channel]);
                def getResp = client.Execute.[List[GetMessageResponse]](getReq);

                unless (getResp.Data == null) {
                    foreach (msg in getResp.Data) {
                        def n = Message();

                        n.Channel = msg.channel_id;
                        n.AuthorName = msg.author?.username ?? "";
                        n.AuthorID = msg.author?.id;
                        n.Content = msg.content ?? "";

                        ret.Add(n);

                        when (msg.id > this.lastIDs[channel]) {
                            this.lastIDs[channel] = msg.id;
                        }
                    }
                }
            }
                
            ret
        }
        
        //Channels:
        //Mirai: 109757750069690368
        //Mirai Ero: 131094472166604801
        //Test: 129180655094464512
        //Test Ero: 131304734228348928 
    }
}
