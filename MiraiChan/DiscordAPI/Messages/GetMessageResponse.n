﻿namespace MiraiChan.DiscordAPI.Messages {
    class GetMessageResponse {
        public author : Author { get; set; }
        public content : string { get; set; }
        public id : long { get; set; }
        public channel_id : long { get; set; }
    }
}
