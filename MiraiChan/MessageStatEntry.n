using System;

namespace MiraiChan {
	[Record]
	class MessageStatEntry {
		public Author : string { get; set; }
		public FirstSeen : DateTime { get; set; }
		public XDCount : int { get; set; }
		public MessageCount : int { get; set; }
		
		public MessagesPerHour : double {
		    get {
		        this.MessageCount / (DateTime.Now - this.FirstSeen).TotalHours   
		    }
		}
		public XDPerHour : double {
		    get {
		        this.XDCount / (DateTime.Now - this.FirstSeen).TotalHours   
		    }
		}
	}
}