﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

namespace MiraiChan.DataAccess {
    [Record]
    class ChannelRecord {
        public ID : long { get; set; }
        public IsAutoQuote : bool { get; set; }
        public IsEro : bool { get; set; }
    }
}
