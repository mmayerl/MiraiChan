﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

namespace MiraiChan.DataAccess {
    [Record]
    class ConfigRecord {
        public Key : string { get; set; }
        public StrValue : string { get; set; }
        public IntValue : int { get; set; }
        public LongValue : long { get; set; }
    }
}
