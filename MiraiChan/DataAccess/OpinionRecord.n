﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

namespace MiraiChan.DataAccess {
    [Record]
    class OpinionRecord {
        public Topic : string { get; set; }
        public Opinion : string { get; set; }
    }
}
