﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

namespace MiraiChan.DataAccess {
    [Record]
    class DictionaryRecord {
        public ID : int { get; set; }
        public Surface : string { get; set; }
        public Reading : string { get; set; }
        public Meaning : string { get; set; }
    }
}
