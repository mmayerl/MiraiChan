﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlServerCe;

namespace MiraiChan.DataAccess {
    class Database {
        private connection : SqlCeConnection;
        
        public this() {
            this.connection = SqlCeConnection("Data Source=data.sdf;Persist Security Info=False;");
            this.connection.Open();
        }
        
        public SelectConfig() : IEnumerable[ConfigRecord] {
            def ret = List();
            
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "SELECT [key], [strvalue], [intvalue], [longvalue] FROM [TblConfig]";
                
                using (reader = com.ExecuteReader()) {
                    while (reader.Read()) {
                        ret.Add(ConfigRecord(reader.GetString(0), reader.GetString(1), reader.GetInt32(2), reader.GetInt64(3)));
                    }
                }
            }
            
            ret
        }
        public UpdateConfigValue(record : ConfigRecord) : void {
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "UPDATE [TblConfig] SET [strvalue] = @sval, [intvalue] = @ival, [longvalue] = @lval WHERE [key] = @k";
                _ = com.Parameters.AddWithValue("@sval", record.StrValue);
                _ = com.Parameters.AddWithValue("@ival", record.IntValue);
                _ = com.Parameters.AddWithValue("@lval", record.LongValue);
                _ = com.Parameters.AddWithValue("@k", record.Key);
                
                _ = com.ExecuteNonQuery();
            }
        }
        
        public SelectRandomQuote(author : string) : QuoteRecord {
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "SELECT TOP(1) id, author, text, added_by FROM TblQuotes WHERE author LIKE @author ORDER BY NEWID()";
                _ = com.Parameters.AddWithValue("@author", "%" + author + "%");
                
                using (reader = com.ExecuteReader()) {
                    if (reader.Read()) {
                        QuoteRecord(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3))
                    }
                    else {
                        null
                    }
                }
            }
        }
        public InsertQuote(record : QuoteRecord) : void {
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "INSERT INTO [TblQuotes] (author, text, added_by) VALUES (@author, @text, @addedby)";
                _ = com.Parameters.AddWithValue("@author", record.Author);
                _ = com.Parameters.AddWithValue("@text", record.Text);
                _ = com.Parameters.AddWithValue("@addedby", record.AddedBy);
                
                _ = com.ExecuteNonQuery();
            }
        }
        
        public SelectRandomAnimeQuote(series : string) : AnimeQuoteRecord {
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "SELECT TOP(1) id, series, episode, text FROM TblAnimeQuotes WHERE series LIKE @series ORDER BY NEWID()";
                _ = com.Parameters.AddWithValue("@series", "%" + series + "%");
                
                using (reader = com.ExecuteReader()) {
                    if (reader.Read()) {
                        AnimeQuoteRecord(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2), reader.GetString(3))
                    }
                    else {
                        null
                    }
                }
            }
        }
        public InsertAnimeQuote(record : AnimeQuoteRecord) : void {
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "INSERT INTO [TblAnimeQuotes] (series, episode, text) VALUES (@series, @episode, @text)";
                _ = com.Parameters.AddWithValue("@series", record.Series);
                _ = com.Parameters.AddWithValue("@episode", record.Episode);
                _ = com.Parameters.AddWithValue("@text", record.Text);
                
                _ = com.ExecuteNonQuery();
            }
        }
        
        public SelectDictionaryEntries(searchterm : string) : IEnumerable[DictionaryRecord] {
            SelectDictionaryEntriesExact(searchterm + "%")
        }
        public SelectDictionaryEntriesExact(searchterm : string) : IEnumerable[DictionaryRecord] {
            def ret = List();
            
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "SELECT id, surface, reading, meaning FROM TblDictionary WHERE surface LIKE @search OR reading LIKE @search OR meaning LIKE @search";
                _ = com.Parameters.AddWithValue("@search", searchterm);
                
                using (reader = com.ExecuteReader()) {
                    while (reader.Read()) {
                        ret.Add(DictionaryRecord(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3)));
                    }
                }
            }
            
            ret
        }
        public InsertDictionaryEntry(record : DictionaryRecord) : void {
              using (com = this.connection.CreateCommand()) {
                com.CommandText = "INSERT INTO [TblDictionary] (surface, reading, meaning) VALUES (@surface, @reading, @meaning)";
                _ = com.Parameters.AddWithValue("@surface", record.Surface);
                _ = com.Parameters.AddWithValue("@reading", record.Reading);
                _ = com.Parameters.AddWithValue("@meaning", record.Meaning);
                
                _ = com.ExecuteNonQuery();
            } 
        }
        
        public SelectPraisesAndInsults(person : string) : PraisesAndInsultsRecord {
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "SELECT TOP(1) person, insults, praises FROM TblPraisesAndInsults WHERE person = @person";
                _ = com.Parameters.AddWithValue("@person", person);
                
                using (reader = com.ExecuteReader()) {
                    if (reader.Read()) {
                        PraisesAndInsultsRecord(reader.GetString(0), reader.GetInt32(1), reader.GetInt32(2))
                    }
                    else {
                        null
                    }
                }
            }
        }
        public UpdatePraisesAndInsults(record : PraisesAndInsultsRecord) : void {
            using (com1 = this.connection.CreateCommand()) {
                com1.CommandText = "SELECT COUNT(*) FROM TblPraisesAndInsults WHERE person = @person";
                _ = com1.Parameters.AddWithValue("@person", record.Person);
                def rows = com1.ExecuteScalar() :> int;
                
                using (com2 = this.connection.CreateCommand()) {
                    com2.CommandText = match (rows) {
                        | 0 => "INSERT INTO TblPraisesAndInsults (person, insults, praises) VALUES (@person, @insults, @praises)"
                        | 1 => "UPDATE TblPraisesAndInsults SET insults = @insults, praises = @praises WHERE person = @person"
                        | _ => throw Exception()
                    };
                    
                    _ = com2.Parameters.AddWithValue("@person", record.Person);
                    _ = com2.Parameters.AddWithValue("@insults", record.Insults);
                    _ = com2.Parameters.AddWithValue("@praises", record.Praises);
                    
                    _ = com2.ExecuteNonQuery();
                }
            }
        }
        
        public SelectOpinion(topic : string) : OpinionRecord {
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "SELECT topic, opinion FROM TblOpinions WHERE topic = @topic";
                _ = com.Parameters.AddWithValue("@topic", topic);
                
                using (reader = com.ExecuteReader()) {
                    if (reader.Read()) {
                        OpinionRecord(reader.GetString(0), reader.GetString(1))
                    }
                    else {
                        null
                    }
                }
            }
        }
        
        public SelectReminders() : IEnumerable[ReminderRecord] {
            def ret = List();
            
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "SELECT id, channel, at, text, [from] FROM TblReminder";

                using (reader = com.ExecuteReader()) {
                    while (reader.Read()) {
                        ret.Add(ReminderRecord(reader.GetInt32(0), reader.GetInt64(1), reader.GetDateTime(2), reader.GetString(3), reader.GetString(4)));
                    }
                }
            }
            
            ret
        }
        public InsertReminder(record : ReminderRecord) : void {
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "INSERT INTO TblReminder (channel, at, text, [from]) VALUES (@channel, @at, @text, @f)";
                _ = com.Parameters.AddWithValue("@channel", record.Channel);
                _ = com.Parameters.AddWithValue("@at", record.At);
                _ = com.Parameters.AddWithValue("@text", record.Text);
                _ = com.Parameters.AddWithValue("@f", record.From);
                
                _ = com.ExecuteNonQuery();
            }
        }
        public DeleteReminder(id : int) : void {
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "DELETE FROM TblReminder WHERE id = @id";
                _ = com.Parameters.AddWithValue("@id", id);
                
                _ = com.ExecuteNonQuery();
            }
        }
        
        public SelectChannels() : IEnumerable[ChannelRecord] {
            def ret = List();
            
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "SELECT id, is_autoquote, is_ero FROM TblChannels";
                
                using (reader = com.ExecuteReader()) {
                    while (reader.Read()) {
                        ret.Add(ChannelRecord(reader.GetInt64(0), reader.GetBoolean(1), reader.GetBoolean(2)));
                    }
                }
            }
            
            ret
        }
        public InsertChannel(record : ChannelRecord) : void {
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "INSERT INTO TblChannels (id, is_autoquote, is_ero) VALUES (@id, @is_autoquote, @is_ero)";
                _ = com.Parameters.AddWithValue("@id", record.ID);
                _ = com.Parameters.AddWithValue("@is_autoquote", record.IsAutoQuote);
                _ = com.Parameters.AddWithValue("@is_ero", record.IsEro);
                
                _ = com.ExecuteNonQuery();
            }
        }
        public UpdateChannel(record : ChannelRecord) : void {
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "UPDATE TblChannels SET is_autoquote = @is_autoquote, is_ero = @is_ero WHERE id = @id";
                _ = com.Parameters.AddWithValue("@id", record.ID);
                _ = com.Parameters.AddWithValue("@is_autoquote", record.IsAutoQuote);
                _ = com.Parameters.AddWithValue("@is_ero", record.IsEro);
                
                _ = com.ExecuteNonQuery();
            }
        }
        public DeleteChannel(record : ChannelRecord) : void {
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "DELETE FROM TblChannels WHERE id = @id";
                _ = com.Parameters.AddWithValue("@id", record.ID);
                
                _ = com.ExecuteNonQuery();
            }
        }
        
        public SelectSpecialUsers() : IEnumerable[SpecialUsersRecord] {
            def ret = List();
            
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "SELECT id, is_owner, is_admin, is_blocked FROM TblSpecialUsers";
                
                using (reader = com.ExecuteReader()) {
                    while (reader.Read()) {
                        ret.Add(SpecialUsersRecord(reader.GetInt64(0), reader.GetBoolean(1), reader.GetBoolean(2), reader.GetBoolean(3)));
                    }
                }
            }
            
            ret
        }
        public SelectSpecialUser(id : long) : SpecialUsersRecord {
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "SELECT id, is_owner, is_admin, is_blocked FROM TblSpecialUsers WHERE id = @id";
                _ = com.Parameters.AddWithValue("@id", id);
                
                using (reader = com.ExecuteReader()) {
                    if (reader.Read()) {
                        SpecialUsersRecord(reader.GetInt64(0), reader.GetBoolean(1), reader.GetBoolean(2), reader.GetBoolean(3))
                    }
                    else {
                        null
                    }
                }
            }
        }
        public InsertSpecialUser(record : SpecialUsersRecord) : void {
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "INSERT INTO TblSpecialUsers (id, is_owner, is_admin, is_blocked) VALUES (@id, @is_owner, @is_admin, @is_blocked)";
                _ = com.Parameters.AddWithValue("@id", record.ID);
                _ = com.Parameters.AddWithValue("@is_owner", record.IsOwer);
                _ = com.Parameters.AddWithValue("@is_admin", record.IsAdmin);
                _ = com.Parameters.AddWithValue("@is_blocked", record.IsBlocked);
                
                _ = com.ExecuteNonQuery();
            }
        }
        public UpdateSpecialUser(record : SpecialUsersRecord) : void {
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "UPDATE TblSpecialUsers SET is_owner = @is_owner, is_admin = @is_admin, is_blocked = @is_blocked WHERE id = @id";
                _ = com.Parameters.AddWithValue("@id", record.ID);
                _ = com.Parameters.AddWithValue("@is_owner", record.IsOwer);
                _ = com.Parameters.AddWithValue("@is_admin", record.IsAdmin);
                _ = com.Parameters.AddWithValue("@is_blocked", record.IsBlocked);
                
                _ = com.ExecuteNonQuery();
            }
        }
        public DeleteSpecialUser(record : SpecialUsersRecord) : void {
            using (com = this.connection.CreateCommand()) {
                com.CommandText = "DELETE FROM TblSpecialUsers WHERE id = @id";
                _ = com.Parameters.AddWithValue("@id", record.ID);
                
                _ = com.ExecuteNonQuery();
            }
        }
        
        public ExecuteQuery(query : string) : string {
            mutable res = "";
            
            using (com = this.connection.CreateCommand()) {
                com.CommandText = query;
                
                try {
                    using (reader = com.ExecuteReader()) {
                        for (mutable i = 0; i < reader.FieldCount; i++) {
                            res += reader.GetName(i) + "  |  ";
                        }
                        res += "\n";
                    
                        while (reader.Read()) {
                            for (mutable i = 0; i < reader.FieldCount; i++) {
                                res += reader.GetValue(i).ToString() + "  |  ";
                            }
                            res += "\n";
                        }
                    }
                }
                catch {
                    | _ is SqlCeException => res = "Fehler: Die Abfrage konnte nicht ausgeführt werden."
                }
            }
            
            res
        }
    }
}
