﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;

namespace MiraiChan.DataAccess {
    [Record]
    class ReminderRecord {
        public ID : int { get; set; }
        public Channel : long { get; set; }
        public At : DateTime { get; set; }
        public Text : string { get; set; }
        public From : string { get; set; }
    }
}
