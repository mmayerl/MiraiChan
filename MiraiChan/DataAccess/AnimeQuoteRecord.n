﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

namespace MiraiChan.DataAccess {
    [Record]
    class AnimeQuoteRecord {
        public ID : int { get; set; }
        public Series : string { get; set; }
        public Episode : int { get; set; }
        public Text : string { get; set; }
    }
}
