﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

namespace MiraiChan.DataAccess {
    [Record]
    class SpecialUsersRecord {
        public ID : long { get; set; }
        public IsOwer : bool  { get; set; }
        public IsAdmin : bool { get; set; }
        public IsBlocked : bool { get; set; }
    }
}
