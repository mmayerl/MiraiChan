﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

namespace MiraiChan.DataAccess {
    [Record]
    class PraisesAndInsultsRecord {
        public Person : string { get; set; }
        public Insults : int { get; set; }
        public Praises : int { get; set; }
    }
}
