﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

namespace MiraiChan.DataAccess {
    [Record]
    class QuoteRecord {
        public ID : int { get; set; }
        public Author : string { get; set; }
        public Text : string { get; set; }
        public AddedBy : string { get; set; }
    }
}
