﻿using System;
using System.Collections.Generic;
using System.Linq;
using MiraiChan.DataAccess;

namespace MiraiChan {
    class Config {
        private db : Database;
        
        internal Channels : List[ChannelRecord];
        internal AutoQuoteChannel : long;
        internal Server : string;
        internal MALServer : string;
        
        internal User : string;
        internal MALUser : string;
        
        internal mutable Interval : int;
        internal mutable ReminderInterval : int;
        internal mutable RequestInterval : int;
        
        public this(db : Database) {
            this.db = db;
            this.Channels = List();
            
            //Get config from TblConfig
            def cr = db.SelectConfig();
            
            foreach (rec in cr) {
                match (rec.Key) {
                    | "Server" => this.Server = rec.StrValue  
                    | "MALServer" => this.MALServer = rec.StrValue
                    | "Interval" => this.Interval = rec.IntValue
                    | "ReminderInterval" => this.ReminderInterval = rec.IntValue
                    | "RequestInterval" => this.RequestInterval = rec.IntValue  
                    | "User" => this.User = rec.StrValue  
                    | "MALUser" => this.MALUser = rec.StrValue
                }
            }
            
            //Get channels from TblChannels
            this.Channels.AddRange(db.SelectChannels());
            def aqc = this.Channels.Where(x => x.IsAutoQuote).FirstOrDefault();
            
            when (aqc == null) {
                Console.WriteLine("Error: No auto-quote channel specified. Check config.");
                Environment.Exit(-1);
            }
            
            this.AutoQuoteChannel = aqc.ID;
        }
        
        internal SetInterval(value : int) : void {
            Interval = value;
            this.db.UpdateConfigValue(ConfigRecord("Interval", "", value, 0));
        }
        internal SetReminderInterval(value : int) : void {
            ReminderInterval = value;
            this.db.UpdateConfigValue(ConfigRecord("ReminderInterval", "", value, 0));
        }
        internal SetRequestInterval(value : int) : void {
            RequestInterval = value;
            this.db.UpdateConfigValue(ConfigRecord("RequestInterval", "", value, 0));
        }
        
        internal GetAsString() : string {
            mutable ret = $"Mirai-chans aktuelle Konfiguration:\n    Interval: $(Interval)\n    ReminderInterval: $(ReminderInterval)\n    RequestInterval: $(RequestInterval)\n\nChannels:\n";
            
            foreach (channel in this.Channels) {
                ret += $"    $(channel.ID), AutoQuote: $(channel.IsAutoQuote), Ero: $(channel.IsEro)\n";
            }
            
            ret
        }
    }
}
